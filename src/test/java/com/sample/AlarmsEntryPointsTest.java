package com.sample;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.sample.model.IpAddress;
import com.sample.model.Log;
import com.sample.model.User;

public class AlarmsEntryPointsTest {

	@Test
	public void testRules() {


		ArrayList<IpAddress> maliciousIPs = new ArrayList<IpAddress>();
		
		IpAddress ip = new IpAddress("188.2.106.199");
		IpAddress ip2 = new IpAddress("188.2.106.198");
		IpAddress ip3 = new IpAddress("188.2.106.197");
		testInactiveDays(ip);
		loginAttempt15times5days(ip);
		differentLogin10Seconds(ip,ip2,ip3);
		differentLogin(ip);
		moreThenSevenAntivirusThreats();
		antiVirus(ip);
		bruteForce(ip);
		dosAttack(ip);
		paymentAttack(ip);
		maliciousIpLogin(maliciousIPs);
		logWithMaliciousIp(maliciousIPs);
		
		
		// for testing antivirus threat
		// Log l1 = new Log(ip, "windows", LocalDateTime.now(), "ANTIVIRUS", "WARNING",
		// "e", new User("pantelica", "LOW",95));

		// for dos attack, payment system attack, brute-force attack
		Log l2 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "LOGIN", "WARNING", "check",
				new User("misk1", "LOW", 95));
		Log l3 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(8), "LOGIN", "ERROR", "a",
				new User("misk2", "LOW", 89));
		Log l4 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(40), "PAYMENT", "WARNING", "b",
				new User("misk3", "LOW", 89));
		Log l5 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "ANTIVIRUS", "SUCCESS", "c",
				new User("misk4", "LOW", 89));
		Log l6 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(10), "ANTIVIRUS", "WARNING", "d",
				new User("misk5", "LOW", 89));

	}

	private void logWithMaliciousIp(ArrayList<IpAddress> maliciousIPs) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		IpAddress newip = new IpAddress("192.168.1.1");

		Log l = new Log(newip, "windows", LocalDateTime.now().minusSeconds(40), "ANTIVIRUS", "WARNING", "b",
				new User("misk3", "LOW", 89));
		kSession.insert(newip);
		kSession.insert(l);
		kSession.insert(maliciousIPs);
		kSession.fireAllRules();
		kSession.dispose();
		
	}

	private void moreThenSevenAntivirusThreats() {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		IpAddress newip = new IpAddress("192.168.1.1");
		kSession.insert(newip);
		for(int i =0; i < 10;i++) {
			Log l = new Log(newip, "windows", LocalDateTime.now().minusSeconds(40), "ANTIVIRUS", "WARNING", "b",
					new User("misk3", "LOW", 89));
			kSession.insert(l);
		}
		kSession.fireAllRules();
		kSession.dispose();
		
	}
	
	private void maliciousIpLogin(ArrayList<IpAddress> maliciousIPs) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		IpAddress ip = new IpAddress("1888.2.106.199");
		kSession.insert(ip);
		maliciousIPs.add(ip);
		kSession.insert(maliciousIPs);
		Log l = new Log(ip, "windows", LocalDateTime.now().minusSeconds(40), "LOGIN", "WARNING", "b",
				new User("misk3", "LOW", 89));
		kSession.insert(l);
		kSession.fireAllRules();
		kSession.dispose();
	}
	
	private void differentLogin(IpAddress ip) {
		// TODO Auto-generated method stub
		
	}

	private void paymentAttack(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		for(int i =0; i < 52;i++) {
			Log l = new Log(ip, "windows", LocalDateTime.now().minusSeconds(40), "PAYMENT", "WARNING", "b",
					new User("misk3", "LOW", 89));
			kSession.insert(l);
		}
		kSession.fireAllRules();
		kSession.dispose();
		
	}

	private void dosAttack(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		IpAddress ip2 = new IpAddress("188.2.106.199");
		kSession.insert(ip2);
		for(int i =0; i < 52;i++) {
			Log l = new Log(ip, "windows", LocalDateTime.now().minusSeconds(40), "PAYMENT", "WARNING", "b",
					new User("misk3", "LOW", 89));
			kSession.insert(l);
		}
		
		kSession.fireAllRules();
		kSession.dispose();
		
	}

	private void bruteForce(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules"); 
		for(int i =0; i < 52;i++) {
			Log l = new Log(ip, "windows", LocalDateTime.now().minusSeconds(40), "LOGIN", "WARNING", "b",
					new User("misk3", "LOW", 89));
			kSession.insert(l);
		}
		
		kSession.fireAllRules();
		kSession.dispose();
		
	}

	private void antiVirus(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		Log l5 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "ANTIVIRUS", "SUCCESS", "c",
				new User("misk4", "LOW", 89));
		Log l6 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(10), "ANTIVIRUS", "WARNING", "d",
				new User("misk5", "LOW", 89));
		kSession.insert(ip);
		kSession.insert(l5);
		kSession.insert(l6);
		kSession.fireAllRules();
		kSession.dispose();
	}

	private void differentLogin10Seconds(IpAddress ip,IpAddress ip2,IpAddress ip3) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		kSession.insert("misk1");
		Log l2 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "LOGIN", "SUCCESS", "check",
				new User("misk1", "LOW", 89));
		kSession.insert(l2);
		Log l3 = new Log(ip2, "windows", LocalDateTime.now().minusSeconds(8), "LOGIN", "SUCCESS", "a",
				new User("misk1", "LOW", 89));
		kSession.insert(l3);
		Log l4 = new Log(ip3, "windows", LocalDateTime.now().minusSeconds(8), "LOGIN", "SUCCESS", "a",
				new User("misk1", "LOW", 89));
		kSession.insert(l4);
		kSession.fireAllRules();
		kSession.dispose();
		
	}

	private void testInactiveDays(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		kSession.insert(ip);
		Log l2 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "LOGIN", "WARNING", "check",
				new User("misk1", "LOW", 95));
		kSession.insert(l2);
		kSession.fireAllRules();
		kSession.dispose();
	}

	private void loginAttempt15times5days(IpAddress ip) {
		KieServices kieServices = KieServices.Factory.get();
		KieContainer kContainer = kieServices.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		Log l2 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(5), "LOGIN", "WARNING", "check",
				new User("misk1", "LOW", 89));
		Log l3 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(48), "LOGIN", "WARNING", "a",
				new User("misk2", "LOW", 89));
		Log l4 = new Log(ip, "windows", LocalDateTime.now().minusSeconds(80), "PAYMENT", "WARNING", "b",
				new User("misk3", "LOW", 89));
		kSession.insert(ip);
		kSession.insert(l2);
		kSession.insert(l3);
		kSession.insert(l4);
		kSession.fireAllRules();
		kSession.dispose();
	}

	public void testCase() {
		System.out.println("hello world");
	}
}
