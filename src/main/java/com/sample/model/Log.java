package com.sample.model;

import java.time.LocalDateTime;

public class Log {
	
	public enum LogType{
		ERROR,
		LOGIN,
		ANTIVIRUS,
		PAYMENT
	};
	
	
	public enum Status{
		SUCCESS,
		ERROR,
		WARNING,
		CHECKING
	};
	
	private String text;
	private Status status;
	private LogType type;
	private User user;
	private IpAddress ip;
	private LocalDateTime timestamp;
	private String platform;
	
	
	
	
	

	public Log(String text, Status status, LogType type, User user, IpAddress ip, LocalDateTime timestamp) {
		super();
		this.text = text;
		this.status = status;
		this.type = type;
		this.user = user;
		this.ip = ip;
		this.timestamp = timestamp;
	}






	public Status getStatus() {
		return status;
	}






	public void setStatus(Status status) {
		this.status = status;
	}






	public Log(String text, LogType type, User user,  IpAddress ip, LocalDateTime timestamp) {
		super();
		this.text = text;
		this.type = type;
		this.user = user;
		this.ip = ip;
		this.timestamp = timestamp;
	}






	public IpAddress getIp() {
		return ip;
	}



	public void setIp(IpAddress ip) {
		this.ip = ip;
	}



	public LocalDateTime getTimestamp() {
		return timestamp;
	}



	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}



	public Log() {
	}



	public Log(IpAddress ip, String platform, LocalDateTime parse, String type, String status,
			String text, User user) {
		this.ip = ip;
		this.platform = platform;
		this.timestamp = parse;
		this.type = LogType.valueOf(type);
		this.status = Status.valueOf(status);
		this.text = text;
		this.user = user;
	}






	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public LogType getType() {
		return type;
	}

	public void setType(LogType type) {
		this.type = type;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}
