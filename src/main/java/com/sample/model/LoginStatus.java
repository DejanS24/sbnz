package com.sample.model;

import java.util.Date;

public class LoginStatus {
	
	private String ipAddress;
	private String username;
	private boolean successful;
	private Date timestamp;
	
	public LoginStatus() {
		
	}
	
	public LoginStatus(String ipAddress, String username, boolean successful, Date timestamp) {
		super();
		this.ipAddress = ipAddress;
		this.username = username;
		this.successful = successful;
		this.timestamp = timestamp;
	}
	
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
