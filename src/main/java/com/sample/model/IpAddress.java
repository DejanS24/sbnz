package com.sample.model;

public class IpAddress {
	String ip;
	
	

	public IpAddress() {
		super();
	}

	public IpAddress(String ip) {
		super();
		this.ip = ip;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
