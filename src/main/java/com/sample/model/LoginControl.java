package com.sample.model;

import java.util.HashMap;
import java.util.Map;

public class LoginControl {
	
	private Map<String, LoginStatus> loginAttempts = new HashMap<String, LoginStatus>();
	
	
	
	public Map<String, LoginStatus> getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(Map<String, LoginStatus> loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public void addLogin(LoginStatus loginStatus) {
        loginAttempts.put(loginStatus.getUsername(), loginStatus);
    }

    public void update(LoginStatus loginStatus) {
        loginAttempts.put(loginStatus.getUsername(), loginStatus);
    }
}
