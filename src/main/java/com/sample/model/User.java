package com.sample.model;

public class User {
	
	private enum UserRisk{
		LOW, MODERTE, HIGH, EXTREME
	};
	
	private String username;
	private String password;
	private String ipAddress;
	private int failedAttempts;
	private UserRisk risk;
	private int inactive;
	private int numberOfTriesLast5days;
	
	
	public User() {
	}
	
	
	public User(String username, String password, String ipAddress, int failedAttempts, UserRisk risk, int inactive) {
		super();
		this.username = username;
		this.password = password;
		this.ipAddress = ipAddress;
		this.failedAttempts = failedAttempts;
		this.risk = risk;
		this.inactive = inactive;
	}
	
	public User(String username, String risk, int inactive) {
		super();
		this.username = username;
		System.out.println(risk);
		this.risk = UserRisk.valueOf(risk);
		this.inactive = inactive;
	}
	
	
	public int getNumberOfTriesLast5days() {
		return numberOfTriesLast5days;
	}



	public void setNumberOfTriesLast5days(int numberOfTries) {
		this.numberOfTriesLast5days = numberOfTries;
	}

	
	public int getInactive() {
		return inactive;
	}
	public void setInactive(int inactive) {
		this.inactive = inactive;
	}
	public UserRisk getRisk() {
		return risk;
	}
	public void setRisk(UserRisk risk) {
		this.risk = risk;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public int getFailedAttempts() {
		return failedAttempts;
	}
	public void setFailedAttempts(int failedAttempts) {
		this.failedAttempts = failedAttempts;
	}
	
	
	
}
